import { React, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import {
  useGetItemsByPacklistQuery,
  useGetOneListQuery,
  useUpdateItemMutation,
  useGetWeatherInfoQuery,
  useGetDatesQuery
} from "../services/Travelthreads";
import { Link } from "react-router-dom";

const ListDetail = () => {
    const params = useParams();

    var packingListID = params["id"];

    const [updateItem] = useUpdateItemMutation(packingListID);

    const { data: allPackingListItems } = useGetItemsByPacklistQuery(packingListID);

    const { data: packingList } = useGetOneListQuery(packingListID);

    const { data: allDateLists} = useGetDatesQuery(packingListID)

    const {data: weatherInfo} = useGetWeatherInfoQuery(packingListID)


    const [location, setLocation] = useState({
        city: "",
        state: "",
        country: ""
    })

    const getData = async () => {
        var locationResponse = ""

        if (packingList.city === "" && packingList.state === ""){
            locationResponse = await fetch(
                `https://travel-threads.dec-ct-12.mod3projects.com/api/location/country/details/${packingList.country}`
            );
            if (locationResponse.ok) {
            const data = await locationResponse.json()
            setLocation({
                country: data.name
            })
        }
        } else if (packingList.city === "" && packingList.state !== "") {
            locationResponse = await fetch(
                `https://travel-threads.dec-ct-12.mod3projects.com/api/location/state/details/${packingList.state}`
            );
            if (locationResponse.ok) {
            const data = await locationResponse.json()
            setLocation({
                state: data.name,
                country: data.country
            })
        }
        } else {
            locationResponse = await fetch(
                `https://travel-threads.dec-ct-12.mod3projects.com/api/location/city/details/${packingList.city}`
            );
            if (locationResponse.ok) {
            const data = await locationResponse.json()
            setLocation({
                city: data.name,
                state: data.state,
                country: data.country
            })
        }
        }
    }

    const handleChange = async (e, object) => {
        let is_packed = object.is_packed ? false : true;
        const body = {
        name: object.name,
        quantity: object.quantity,
        is_packed: is_packed,
        packing_list_id: object.packing_list_id,
        date_list_id: object.date_list_id,
        id: object.id,
        };
        await updateItem(body);
    };


    const checkLocationInfo = (location) => {
        if (location.city === undefined && location.state === undefined) {
            return (
                <p>Your trip to beautiful {location.country}.</p>
            )
            } else if (location.city === undefined && location.state !== undefined){
            return (
                <p>Your trip to beautiful {location.state}, {location.country}.</p>
            )
            } else {
            return(
                <p>Your trip to beautiful {location.city}, {location.state}, {location.country}.</p>
            )
        }
    }


    useEffect(() => {
        if (packingList) {
        getData();
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [packingList]);

    const weatherCardStyling = {
        padding: "1em",
        minWidth: "5.5em",
        outlineStyle: "solid",
        outlineWidth: ".4em",
        outlineColor: "#FFA69E",
        outlineOffset: "-5px",
        borderRadius: "10px",
        backgroundColor: "white"
    }

    const weatherWindow = {
        paddingLeft: "0px",
        outlineStyle: "solid",
        outlineWidth: ".2em",
        outlineColor: "#FFA69E",
        overflowX: "scroll",
        whiteSpace: "nowrap",
        backgroundColor: "#FFA69E"
    }

    const weatherWindowBody = {
        marginLeft: "auto",
        marginRight: "auto",
        paddingLeft: "0px"
    }

    const background = {
        minHeight: "100vh",
        height: "100%",
        backgroundImage: "linear-gradient(to bottom right, #AED9E0, #FFA69E)",
        backgroundSizing: "cover",
        padding:"3em",

    }

    const listInfo = {
        padding: "6em",
        marginTop: "0px",
        height: "7em"
    }


    return (
        <div  style={background}>
            <div className="card" style={{backgroundColor: "#5E6472"}}>
                <div className="row" style={listInfo}>
                    <div className="col" style={{color: "white"}}>
                        <h1>{packingList?.name}</h1>
                        {checkLocationInfo(location)}
                    </div>
                    <table className="col-7 card row" style={weatherWindow}>
                        <tbody style={weatherWindowBody}>
                            <tr >
                                {allDateLists?.map((dateList) => {
                                    let weatherCard = <td className="col" key={dateList.date} style={weatherCardStyling}>
                                        <b>{dateList.date.slice(5, 10)}</b> <br/>
                                        H: NA<br/>
                                        L: NA<br/>
                                        &#127783; NA
                                        </td>
                                    if (weatherInfo !== undefined && weatherInfo.daily.time.indexOf(dateList.date) !== -1) {
                                        const index = weatherInfo.daily.time.indexOf(dateList.date)
                                        weatherCard = <td className="col" key={dateList.date} style={weatherCardStyling}>
                                            <b>{dateList.date.slice(5, 10)}</b> <br/>
                                            H: {weatherInfo.daily.temperature_2m_max[index]}<span>&#176;</span> <br/>
                                            L: {weatherInfo.daily.temperature_2m_min[index]}<span>&#176;</span> <br/>
                                            &#127783; {weatherInfo.daily.precipitation_probability_max[index]}%
                                        </td>
                                    }
                                    return weatherCard
                                })}
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div style={{fontSize: "1.25em", padding: "3em"}}>
                    <table className="card table" style={{display: "block"}}>
                        <thead style={{width: "100%"}}>
                            <tr style={{width: "100%"}}>
                                <th style={{ textAlign: "center"}}>Packed</th>
                                <th style={{width: "100%", paddingLeft: "4em"}}>Item Name</th>
                                <th style={{ textAlign: "center"}}>Quantity</th>
                            </tr>
                        </thead>
                        <tbody>
                            {allPackingListItems?.map((item) => {

                                return (
                                    <tr key={item.id}>
                                        <td className="position-relative" style={{}}>
                                            <input
                                                className="position-absolute top-50 start-50 translate-middle form-check-input"
                                                type="checkbox"
                                                defaultChecked={item.is_packed}
                                                onClick={(e) => handleChange(e, item)}>
                                            </input>
                                        </td>
                                        <td style={{ paddingLeft: "4em"}}>{item.name}</td>
                                        <td style={{ textAlign: "center"}}>{item.quantity}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
                <div className="d-flex justify-content-end" style={{paddingRight: "4em", paddingBottom: "1em", justifyContent: "right"}}>
                    <button className="btn btn-primary" >
                        <Link to={`/packinglist/${packingListID}/datelists`} style={{color: "white", textDecoration: "none"}}>Edit List</Link>
                    </button>
                </div>
            </div>
        </div>
    )
}

export default ListDetail
